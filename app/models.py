import uuid
from decimal import Decimal
from typing import Any, List

from pydantic import BaseModel


def format_values(o: Any) -> dict:
    for key, value in o.items():
        if isinstance(value, Decimal):
            o[key] = str(value)
        elif isinstance(value, uuid.UUID):
            o[key] = value.hex
    return o


def to_json(o: object, recurse: bool = True) -> Any:
    if isinstance(o, list) or isinstance(o, set):
        if not len(o):
            return []
        return [to_json(v, recurse) for v in o]
    if isinstance(o, dict):
        return {k: to_json(v, recurse) for k, v in o.items()}
    if isinstance(o, BaseModel):
        return o.dict()
    return o
