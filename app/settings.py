import os

from environs import Env

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
env = Env()
env.read_env(os.path.join(BASE_DIR, ".env"), override=False)

MONGO_USERNAME = env.str("MONGO_USERNAME", default="")
MONGO_PASSWORD = env.str("MONGO_PASSWORD", default="")
MONGO_DOMAIN = env.str("MONGO_DOMAIN", default="")
MONGO_PORT = env.int("MONGO_PORT", default="")
AUTH_HASH = env.str("AUTH_HASH", default="")
# Logging
LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", default="INFO").upper()
LOGGING_FORMAT = (
    "%(asctime) %(created) %(filename) %(funcName) %(levelname) "
    "%(lineno) %(module) %(message) %(name) %(thread) %(threadName)"
)
LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "json": {
            "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
            "format": LOGGING_FORMAT,
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "json",
            "level": LOGGING_LEVEL,
        }
    },
    "loggers": {
        "celery": {"handlers": ["console"], "level": LOGGING_LEVEL, "propagate": True}
    },
}

BASE_URL_V1 = "/api/v1/"

ACTIVATION_CODE_LENGTH = 6
