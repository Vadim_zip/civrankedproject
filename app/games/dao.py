from typing import List

from app import settings
from app.mongo.db import get_database
from app.schemas import Game


class GamesDao:
    db = get_database()
    collection = db["games"]

    @classmethod
    def insert_game(cls, game: Game):
        cls.collection.insert_one(game.dict(by_alias=True))

    @classmethod
    def get_games_by_id(cls, game_id) -> List[Game]:
        games = [Game(**game) for game in cls.collection.find({"game_id": int(game_id)})]
        return games

    @classmethod
    def get_games(cls):
        games = [Game(**game) for game in cls.collection.find()]
        return games
