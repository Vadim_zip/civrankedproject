import falcon
from peewee_validates import Validator, StringField, BooleanField, IntegerField

from app import settings
from app.games.utils import GamesUtils
from app.models import to_json
from app.schemas import Game


class GamesResource:
    @staticmethod
    def on_get(req: falcon.Request, resp: falcon.Response) -> None:
        games = GamesUtils.get_all_games()
        resp.json = to_json({"data": {"success": True, "games": games}})

    @staticmethod
    def on_post(req: falcon.Request, resp: falcon.Response) -> None:

        auth = req.headers.get('AUTHORIZATION', None)
        if auth is None or auth != settings.AUTH_HASH:
            raise falcon.HTTPBadRequest(description="Unauthorized")
        validator = GameValidator()
        if not validator.validate(req.json):  # pragma: no cover
            raise falcon.HTTPBadRequest("Validation error", validator.errors)
        _id = validator.data.get("_id")
        game_id = validator.data.get("game_id")
        steam_id = validator.data.get("steam_id")
        ds_id = validator.data.get("ds_id")
        score = validator.data.get("score")
        turn_count = validator.data.get("turn_count")
        is_winner = validator.data.get("is_winner")
        game = Game(
            _id=_id,
            game_id=game_id,
            steam_id=steam_id,
            ds_id=ds_id,
            score=score,
            turn_count=turn_count,
            is_winner=is_winner,
        )
        success = GamesUtils.save_game(game)

        resp.json = to_json({"data": {"success": success}})


class GameResource:
    @staticmethod
    def on_get(req: falcon.Request, resp: falcon.Response, game_id: int) -> None:
        games = GamesUtils.get_games_by_game_id(game_id)
        resp.json = to_json({"data": {"success": True, "games": games}})


class GameValidator(Validator):
    _id = IntegerField()
    game_id = IntegerField()
    steam_id = StringField()
    ds_id = StringField()
    score = IntegerField()
    turn_count = IntegerField()
    is_winner = BooleanField()
