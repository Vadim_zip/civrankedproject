from typing import List

from app.games.dao import GamesDao
from app.schemas import Game


class GamesUtils:
    @classmethod
    def save_game(cls, game: Game) -> bool:
        try:
            GamesDao.insert_game(game)
            return True
        except Exception as _:
            return False

    @classmethod
    def get_games_by_game_id(cls, game_id) -> List[Game]:
        games = GamesDao.get_games_by_id(game_id)
        return games

    @classmethod
    def get_all_games(cls):
        games = GamesDao.get_games()
        return games
