from pydantic import BaseModel, Field


class Game(BaseModel):
    id: str = Field(..., alias='_id')
    game_id: int
    steam_id: str = None
    ds_id: str
    score: int
    turn_count: int
    is_winner: bool
