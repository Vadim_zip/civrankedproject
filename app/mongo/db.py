import logging

from pymongo import MongoClient

from app import settings

client = MongoClient(
    host=[str(settings.MONGO_DOMAIN) + ":" + str(settings.MONGO_PORT)],
    serverSelectionTimeoutMS=3000,
    username=settings.MONGO_USERNAME,
    password=settings.MONGO_PASSWORD,
)


def get_database():
    return client['civ6_database']
