import falcon_jsonify
from falcon import App

from app import settings
from app.cors import CORSComponent
from app.games.resources import GamesResource, GameResource

V1 = settings.BASE_URL_V1

application = App(
    middleware=[
        CORSComponent(),
        falcon_jsonify.Middleware(help_messages=True),
    ]
)

application.add_route(f"{V1}games", GamesResource())
application.add_route(f"{V1}game/{{game_id}}", GameResource())
