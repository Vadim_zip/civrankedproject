import falcon


class CORSComponent(object):
    def process_response(
        self,
        req: falcon.Request,
        resp: falcon.Response,
        resource: object,
        req_succeeded: bool,
    ) -> None:
        resp.set_header("Access-Control-Allow-Origin", "*")

        allow = "DELETE, GET, OPTIONS, PATCH, POST, PUT"
        resp.delete_header("Allow")

        allow_headers = ", ".join(
            [
                "accept",
                "accept-encoding",
                "authorization",
                "content-type",
                "dnt",
                "dt",
                "origin",
                "user-agent",
                "x-csrftoken",
                "x-requested-with",
            ]
        )

        resp.set_headers(
            (
                ("Access-Control-Allow-Methods", allow),
                ("Access-Control-Allow-Headers", allow_headers),
                ("Access-Control-Max-Age", "86400"),  # 24 hours
            )
        )
