# install stage
FROM python:3.7.12-slim
WORKDIR /app
# set python environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1
# install dependencies
RUN pip3 install --no-cache-dir \
    pipenv
COPY Pipfile Pipfile.lock ./
RUN PIP_NO_CACHE_DIR=off pipenv install --deploy

COPY . .
EXPOSE 5000
RUN chmod +x docker-entrypoint.sh
ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["pipenv", "run", "python3", "/app/run_server.py"]
