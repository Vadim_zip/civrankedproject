import argparse
from waitress import serve
import logging
logger = logging.getLogger('waitress')
logger.setLevel(logging.INFO)
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", type=str, default="5000")
    input_args = parser.parse_args()
    from app.service import application

    serve(application, listen=f"*:{input_args.port}")
